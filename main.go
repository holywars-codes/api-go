package main

import (
	"api-go/hand_f"
	"api-go/handlers"
	"api-go/services"
	"github.com/gofiber/fiber/v2"
	"github.com/labstack/echo/v4"
	_ "github.com/lib/pq"
	"log"
)

func main() {
	var postService services.PostService
	serv := services.New(postService)

	if true {
		e := echo.New()

		handler := handlers.NewPostHandler(serv.PostService())
		e.GET("/posts", handler.GetPosts)
		e.GET("/post/:id", handler.GetPostById)
		e.GET("/a/post/:id", handler.GetPostRawById)
		e.POST("/post", handler.CreatePost)
		e.Logger.Fatal(e.Start(":8080"))
	} else {
		app := fiber.New()

		handler := hand_f.NewPostHandler(serv.PostService())
		app.
			Get("/posts", handler.GetPosts).
			Get("/post/:id", handler.GetPostById).
			Get("/a/post/:id", handler.GetPostRawById).
			Post("/post", handler.CreatePost)

		log.Fatal(app.Listen(":8080"))
	}
}
