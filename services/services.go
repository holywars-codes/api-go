package services

import (
	"api-go/models"
	"github.com/jmoiron/sqlx"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type Services struct {
	postService PostService
	db          *gorm.DB
}

func New(postService PostService) *Services {
	dsn := "host=localhost user=postgres password=camunda dbname=gorm_demo port=5432 sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent),
	})
	if err != nil {
		panic(err.Error())
	}
	errMigrations := db.AutoMigrate(&models.Post{})
	if errMigrations != nil {
		panic(errMigrations.Error())
	}

	connect, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		panic(err.Error())
	}
	postService.InjectDB(db, connect)

	return &Services{
		postService: postService,
		db:          db,
	}
}

func (s *Services) PostService() *PostService {
	return &s.postService
}
