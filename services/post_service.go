package services

import (
	"api-go/models"
	"github.com/jmoiron/sqlx"
	"gorm.io/gorm"
)

type PostService struct {
	db  *gorm.DB
	sdb *sqlx.DB
}

func (s *PostService) InjectDB(db *gorm.DB, sdb *sqlx.DB) {
	s.db = db
	s.sdb = sdb
}

func (s *PostService) GetPosts(limit int, offset int) []models.Post {
	var posts []models.Post
	s.db.Model(models.Post{}).Limit(limit).Offset(offset).Find(&posts)
	return posts
}

func (s *PostService) GetById(id uint64) models.Post {
	var post models.Post
	s.db.Model(&models.Post{ID: id}).First(&post)
	return post
}

func (s *PostService) GetRwaById(id uint64) (*models.PostSQL, error) {
	var post models.PostSQL
	err := s.sdb.Get(&post, "SELECT id, title, body, published from posts where id=$1", id)
	if err != nil {
		return nil, err
	}
	return &post, nil
}

func (s *PostService) CreatePost(form models.PostForm) models.Post {
	post := models.Post{Body: form.Body, Title: form.Title, Published: form.Published}
	s.db.Create(&post)
	return post

}
