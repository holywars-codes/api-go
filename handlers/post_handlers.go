package handlers

import (
	"api-go/models"
	"api-go/services"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

type PostHandler struct {
	postService *services.PostService
}

func NewPostHandler(postService *services.PostService) *PostHandler {
	return &PostHandler{postService: postService}
}

func (h *PostHandler) CreatePost(c echo.Context) error {
	//body, _ := io.ReadAll(c.Request().Body)
	var postForm models.PostForm
	//err := json.Unmarshal(body, &postForm)
	//if err != nil {
	//	return err
	//}

	if errBinding := c.Bind(&postForm); errBinding != nil {
		return errBinding
	}

	post := h.postService.CreatePost(postForm)
	return c.JSON(http.StatusCreated, post)
}

func (h *PostHandler) GetPosts(c echo.Context) error {
	limit, err1 := strconv.Atoi(c.QueryParam("limit"))
	offset, err2 := strconv.Atoi(c.QueryParam("offset"))
	if err1 != nil || err2 != nil {
		return c.JSON(http.StatusBadRequest, "wrong limit or/and offset")
	}
	return c.JSON(http.StatusOK, h.postService.GetPosts(limit, offset))
}

func (h *PostHandler) GetPostById(c echo.Context) error {
	id, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "wrong post id")
	}
	return c.JSON(http.StatusOK, h.postService.GetById(id))
}

func (h *PostHandler) GetPostRawById(c echo.Context) error {
	id, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "wrong post id")
	}
	post, sqlErr := h.postService.GetRwaById(id)
	if sqlErr != nil {
		return c.JSON(http.StatusNotFound, sqlErr.Error())
	}
	return c.JSON(http.StatusOK, post)
}
