package models

import (
	"gorm.io/gorm"
	"time"
)

type Post struct {
	//gorm.Model
	CreatedAt time.Time      `json:"createdAt"`
	UpdatedAt time.Time      `json:"updatedAt"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deletedAt"`
	ID        uint64         `gorm:"primarykey" json:"id"`
	Title     string         `gorm:"sie:64" json:"title"`
	Body      string         `json:"body"`
	Published bool           `json:"published"`
}

type PostSQL struct {
	ID        uint64 `db:"id" json:"id"`
	Title     string `db:"title" json:"title"`
	Body      string `db:"body" json:"body"`
	Published bool   `db:"published" json:"published"`
}

type PostForm struct {
	Title     string `json:"title"`
	Body      string `json:"body"`
	Published bool   `json:"published"`
}
