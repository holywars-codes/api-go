package hand_f

import (
	"api-go/models"
	"api-go/services"
	"github.com/gofiber/fiber/v2"
	"net/http"
)

type PostHandler struct {
	postService *services.PostService
}

func NewPostHandler(postService *services.PostService) *PostHandler {
	return &PostHandler{postService: postService}
}

func (h *PostHandler) CreatePost(c *fiber.Ctx) error {
	//body, _ := io.ReadAll(c.Request().Body)
	var postForm models.PostForm
	//err := json.Unmarshal(body, &postForm)
	//if err != nil {
	//	return err
	//}

	if errBinding := c.BodyParser(&postForm); errBinding != nil {
		return errBinding
	}

	post := h.postService.CreatePost(postForm)
	return c.Status(http.StatusOK).JSON(post)
}

func (h *PostHandler) GetPosts(c *fiber.Ctx) error {
	limit := c.QueryInt("limit")
	offset := c.QueryInt("offset")
	if limit == 0 {
		return c.Status(http.StatusBadRequest).JSON("wrong limit or/and offset")
	}
	return c.Status(http.StatusOK).JSON(h.postService.GetPosts(limit, offset))
}

func (h *PostHandler) GetPostById(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	if err != nil || id <= 0 {
		return c.Status(http.StatusBadRequest).JSON("wrong post id")
	}
	return c.Status(http.StatusOK).JSON(h.postService.GetById(uint64(id)))
}

func (h *PostHandler) GetPostRawById(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	if err != nil || id <= 0 {
		return c.Status(http.StatusBadRequest).JSON("wrong post id")
	}
	post, sqlErr := h.postService.GetRwaById(uint64(id))
	if sqlErr != nil {
		return c.Status(http.StatusNotFound).JSON(sqlErr)
	}
	return c.Status(http.StatusOK).JSON(post)
}
